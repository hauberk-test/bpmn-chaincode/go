
# Chaincode configuration

[`config.yaml`](/config.yaml) is main chaincode configuration artefact. It desribes all aspects of chaincode

[`chaincode.puml`](/chaincode.puml) is applied program interface of the smart contract

[`order-state.puml`](/order-state.puml) is state-flow diagram for entity *Order*

[`invoice-state.puml`](/invoice-state.puml) is state-flow diagram for entity *Invoice*

[`invoice.bpmn`](/invoice.bpmn) is BPMN-model for RequestService interface of smart contract
